---
layout: markdown_page
title: Product Vision - Manage
---

## On this page
{:.no_toc}

- TOC
{:toc}

This is the product vision for Manage in 2019 and beyond. If you'd like to discuss this vision directly with the product manager for Manage, feel free to reach out to Jeremy Watson via [e-mail](mailto:jwatson@gitlab.com), [Twitter](https://twitter.com/gitJeremy), or by [scheduling a video call](https://calendly.com/jeremy_/gitlab-product-chat).

## Overview

For administrators and executives, the process of management is always on. It extends to managing people, money, and risk; when the stakes are high, these stakeholders demand an experience and feature set that makes them feel in control. Setting up your processes shouldn’t be a struggle, and administrators shouldn’t have to compromise on security or compliance to make software work for them.

Not only do we want to fulfill those fundamental needs, we want to give you the freedom to work in new and powerful ways. We aspire to answer questions managers didn't know they had, and to automate away the mundane.

Manage's role in GitLab is to **help organizations prosper with configuration and analytics that enables them to work more efficiently**. It’s not enough to give instances the ability to meet their most basic needs; as a single application for the DevOps lifecycle, GitLab can exceed the standard and enable you to work in ways you previously couldn’t.

## Our vision

We’re realizing this vision in 2019 by delivering more powerful insights, giving you the power to automate custom workflows, and iterating on must-have features for complex organizations.

### 🔔 Analytics and insights

As instances thrive, the amount of information flowing through GitLab grows exponentially. An administrator’s job quickly becomes more reliant on automation and tools to help them stay reactive (I need to respond to an urgent request for information) and proactive (tell me about areas of risk).

In 2019, we'll help GitLab instances excel at both:
- Changes in GitLab should be fully traceable. After code gets merged, it may involve a host of individuals, commits, and objects - while we make it easy to go from idea to production, tracing that history back should also be a cinch. Since the heart of code change in GitLab is the merge request, we’ll add the ability to see the a deep history - including the issues, people, and commits - that led to the change. By building monitoring and traceability deep into the application, our goal is to let GitLab thrive in any regulated environment.
- We'll find new opportunities to tell you something insightful and new about how you’re shipping software. We're doubling down on the power of analytics to provide insight into how instances can reduce cycle time and ship faster. 

### 🛠 Effortless workflows

GitLab’s [single application](https://about.gitlab.com/handbook/product/#single-application) approach to DevOps puts the entire software development lifecycle in a single place. As a result, users don’t have to stitch together an overly-fragmented toolchain - and we want to go deeper in on that advantage.

GitLab is built to be inherently flexible, as organizations have a variety of different workflows and structures. Instead of strictly representing these workflows with manual handoffs, we want to automate the processes that define your organization. In the same way that you’re able to define a CI pipeline, we want to bring this concept to how you use the rest of the application - end-to-end.

### 🚢 Managing at scale

As GitLab continues to grow, we want to continue to iterate and improve on existing features. This is especially true of features that help large organizations thrive in GitLab.

We’re continuing to improve on authentication within GitLab, which is of critical importance for managing users at scale. We’re continuing to build out Group SAML for GitLab.com by automating membership and permissions management. We’re also improving OAuth by allowing you to programmatically manage tokens.

Finally, we’re excited to give instances more control and power over how they manage spending. You’ll be able to clearly understand how your instance’s license is being used, with granular control over seats. Alongside making billing easier to understand than ever, we’re also improving our billing portal to give you the power to self-serve changes to your subscription.

## Upcoming

- Q4 2018
  - 🚢 [Automatic provisioning/deprovisioning for Group SAML](https://gitlab.com/groups/gitlab-org/-/epics/95)
  - 🚢 [Redesigned billing areas](https://gitlab.com/gitlab-org/gitlab-ee/issues/6898)
  - [On-demand shared runner minutes](https://gitlab.com/gitlab-org/gitlab-ee/issues/3314)
- Q1 2019
  - 🛠 [New onboarding](https://gitlab.com/gitlab-org/gitlab-ee/issues/1869)
  - 🚢 [SSO enforcement for Group SAML](https://gitlab.com/groups/gitlab-org/-/epics/94)
  - 🛠 [Personal smart dashboard](https://gitlab.com/gitlab-org/gitlab-ce/issues/27111)
  - [Follow a user](https://gitlab.com/gitlab-org/gitlab-ce/issues/52739)
- Q2 2019
  - 🚢 [LDAP for Groups](https://gitlab.com/gitlab-org/gitlab-ee/issues/8047)
  - [On-demand storage](https://gitlab.com/gitlab-org/gitlab-ee/issues/5634)
  - 🔔 [Code analytics](https://gitlab.com/groups/gitlab-org/-/epics/368)
- Q3 2019
  - [Developer timeline](https://gitlab.com/gitlab-org/gitlab-ee/issues/8106)
  - 🚢🛠 [Automations](https://gitlab.com/groups/gitlab-org/-/epics/218)
  - 🔔 Full traceability for group and projects settings and actions
  - 🔔 [Behavioral monitoring](https://gitlab.com/groups/gitlab-org/-/epics/259)
- Q4 2019
  - 🔔 [Idea-to-production traceability](https://gitlab.com/gitlab-org/gitlab-ee/issues/6761)
  - 🚢🛠 [Policies](https://gitlab.com/groups/gitlab-org/-/epics/366)

## How we prioritize

We follow the same prioritization guidelines as the [product team at large](https://about.gitlab.com/handbook/product/#prioritization). Issues tend to flow from having no milestone, to being added to the backlog, to a directional milestone (e.g. Next 3-4 releases), and are finally assigned a specific milestone.

Our entire public backlog for Manage can be viewed [here](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3Amanage), and can be filtered by labels or milestones. If you find something you are interested in, you're encouraged to jump into the conversation and participate. At GitLab, everyone can contribute!

You can see further details on the prioritization and development process on the [page for the Manage team](https://about.gitlab.com/handbook/product/categories/manage/).

## Our plans

We couldn’t be more excited to make GitLab easier than ever to manage. In 2019, we’ll do this by building out powerful configuration that reflects your unique needs and providing you with insightful analytics that helps you move faster than ever.

We’ll keep iterating on these concepts well into 2020, and continue adding more powerful analytics and collaboration features to help you work seamlessly in GitLab.

## Direction

<%= direction %>

<%= partial("direction/other", :locals => { :stage => "manage" }) %>
