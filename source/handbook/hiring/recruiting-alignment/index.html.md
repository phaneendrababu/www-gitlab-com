---
layout: markdown_page
title: "Recruiting Alignment"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Recruiter and Coordinator Alignment by Hiring Manager

| Sales                    | Recruiter       | Coordinator                                |
|--------------------------|-----------------|--------------------------------------------|
| Richard Pidgeon          | Nadia Vatalidis | K.A. (to be hired, Emily Mowry as interim) |
| Chad Malchow             | Kelly Murdock   | K.A. (to be hired, Emily Mowry as interim) |
| Michael Alessio          | Nadia Vatalidis | K.A. (to be hired, Emily Mowry as interim) |
| Francis Aquino           | Kelly Murdock   | K.A. (to be hired, Emily Mowry as interim) |
| Paul Almeida             | Kelly Murdock   | K.A. (to be hired, Emily Mowry as interim) |
| Kristen Lawrence         | Kelly Murdock   | K.A. (to be hired, Emily Mowry as interim) |
| Leslie Blanchard         | Kelly Murdock   | K.A. (to be hired, Emily Mowry as interim) |

| Marketing                                      | Recruiter       | Coordinator                                |
|------------------------------------------------|-----------------|--------------------------------------------|
| Erica Lindberg                                 | Jacie Zoerb     | K.A. (to be hired, Emily Mowry as interim) |
| Ashish Kuthiala                                | Jacie Zoerb     | K.A. (to be hired, Emily Mowry as interim) |
| LJ Banks                                       | Jacie Zoerb     | K.A. (to be hired, Emily Mowry as interim) |
| Alex Turner                                    | Jacie Zoerb     | K.A. (to be hired, Emily Mowry as interim) |
| Elsje Smart                                    | Nadia Vatalidis | K.A. (to be hired, Emily Mowry as interim) |
| Director of Field Marketing (to be hired, Leslie Blanchard as interim) | Jacie Zoerb     | K.A. (to be hired, Emily Mowry as interim) |
| Director of Marketing Operations (to be hired, LJ Banks as interim) | Jacie Zoerb     | K.A. (to be hired, Emily Mowry as interim) |
| David Planella                                 | Jacie Zoerb     | K.A. (to be hired, Emily Mowry as interim) |

| Engineering           | Recruiter                                               | Coordinator |
|-----------------------|---------------------------------------------------------|-------------|
| Tim Zallmann          | Eva Petreska (to be hired, Nadia Vatalidis as interim)  | Emily Mowry |
| Sarrah Vesselov       | Eva Petreska (to be hired, Chloe Whitestone as interim) | Emily Mowry |
| Kathy Wang            | Steve Pestorich                                         | Emily Mowry |
| Tommy Morgan          | Trust Ogor                                              | Emily Mowry |
| Mek Stittri           | Eva Petreska (to be hired, Nadia Vatalidis as interim)  | Emily Mowry |
| Dalia Havens          | Trust Ogor                                              | Emily Mowry |
| Tom Cooney            | Nadia Vatalidis                                         | Emily Mowry |
| Gerir Lopez-Fernandez | Steve Pestorich                                         | Emily Mowry |

| Product           | Recruiter                       | Coordinator |
|-------------------|---------------------------------|-------------|
| Mark Pundsack     | Nadia Vatalidis/Steve Pestorich | Emily Mowry |
| Job van der Voort | Nadia Vatalidis                 | Emily Mowry |

| Other         | Recruiter                       | Coordinator                                |
|---------------|---------------------------------|--------------------------------------------|
| Paul Machle   | Jacie Zoerb/Nadia Vatalidis     | K.A. (to be hired, Emily Mowry as interim) |
| Barbie Brewer | Jacie Zoerb/Nadia Vatalidis     | K.A. (to be hired, Emily Mowry as interim) |
| Brandon Jung  | Kelly Murdock                   | K.A. (to be hired, Emily Mowry as interim) |
| Meltano       | Steve Pestorich/Nadia Vatalidis | K.A. (to be hired, Emily Mowry as interim) |

## Sourcer Alignment by Division and Location

| Product               | Region            | Sourcer                | Estimated % |
|-----------------------|-------------------|------------------------|-------------|
| Sales & Marketing     | Americas/Anywhere | Stephanie Garza        | 100%        |
| Sales & Marketing     | EMEA & APAC       | Anastasia Pshegodskaya | 25%         |
| Engineering Backend   | Anywhere          | Zsuzsanna Kovacs       | 100%        |
| Engineering General   | Anywhere          | Anastasia Pshegodskaya | 50%         |
| Other/Director+ Roles | Anywhere          | Anastasia Pshegodskaya | 25%         |
