---
layout: markdown_page
title: "Evangelist Program"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Overview

## Organizing meetups

- We love and support Meet-ups. If you would like someone from the GitLab team to stop by your event or might be interested in having GitLab as a sponsor please email `evangelists@gitlab.com`. Please note, we will be much more able to assist if given sufficient lead time (at least a month) to evaluate your request, process payment, produce merch, etc.
- GitLab Meet-ups: Ideally, the first couple of meet-ups should be run by GitLab employees, but once someone manages to have a couple successive events, the meet-up itself will live on. It is much harder to start new meet-ups versus maintaining existing ones. So we make an effort to support and keep existing events going.
- If you are interested in creating your own GitLab Meet-Up, please create a Meet-Up account and set at least 2 meetings. Your first meeting with a date and topic, and then a second meeting with a date and topic. The second meeting can have a flexible topic based on how the first meeting goes. The point of setting two meetings is to help build momentum in the new group.  You can find the list of GitLab meetups on the [meetup.com page](https://www.meetup.com/topics/gitlab/).
- Reach out to other like-topic Meet-Ups and invite them to your Meet-Up.
- Once you have scheduled your Meet-Up, add the event to our [events page](/events/) so the whole world knows! Check out the [How to add an event to the events .yml](/handbook/marketing/corporate-marketing/#how-to-add-an-event-to-the-eventsyml/) section if you need help on how to add an event.
- If you purchase any food & beverage for the meetup event, we can help reimburse the expense.  A general guideline is $US 5/person for a maximum of $US 500 per each meetup. You will be asked to provide receipts, attendees list/photo, etc.  If you have questions or need help with food & beverage reimbursements, please email `evangelists@gitlab.com`.

## Guidelines

## Workflows

- [Merchandise](/handbook/marketing/community-relations/evangelist-program/workflows/merchandise.html)
- [Find a speaker](/handbook/marketing/community-relations/evangelist-program/workflows/find-a-speaker.html)

## Operations

- [Merchandise](/handbook/marketing/community-relations/evangelist-program/ops/merchandise.html)
