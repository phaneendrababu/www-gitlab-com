---
layout: job_page
title: "Corporate Events Manager"
---

The Corporate Events manager will deliver extraordinary experiences that leave a lasting impression and deep commitment to our brand, culture, and purpose. They will work closely with the People Operations team and organizational leaders in executing GitLab's most significant events globally, including but not limited to our company summit, speaking engagements, industry events, meet-ups, etc.

They will also lead the planning efforts for the company summit, which brings our global team together for a meaningful experience of learning, collaboration, teamwork, and fun. This includes the logistics planning, event communication, site selection, and setup, as well as hotel, travel, and transportation booking. 


## Responsibilities

- Manage all aspects of event logistics from the development of content, production, venue management, promotions, and other event-related marketing responsibilities.
- Analyze programs for effectiveness and conduct post-event reporting.
- Interact with and manage multiple vendors, partners, company executives, and a wide range of cross-functional groups within GitLab.
- Represent GitLab at industry events and conferences.
- Develop and execute robust team building activities that promote team member and community bonding.
- Research industry events and make recommendations on upcoming opportunities and speaking engagements.
- After each event, provide a recap of what worked and what didn’t so we can continue to improve processes.
- Manage event budgets and calendars.

## Requirements

- Bachelor's degree required.
- Experience planning internal and external events with attendance greater than 500.
- 5+ years of event planning, preferably in the tech space and for a high growth startup.
- Professional experience planning and booking domestic and international travel and accommodations.
- Experience coordinating logistics for conference and industry events.
- Experience building and maintaining internal and external partnerships.
- Excellent time management and project management skills.
- Resourceful self-starter with a sense of urgency and a strong work ethic.
- Superior verbal and written communication skills.
- Excellent budget management, negotiation, organizational, and presentation skills.
- Effective written and verbal communication skills and excellent interpersonal skills.

## Hiring Process

- Qualified candidates will be invited to schedule a 30 minute screening call with one of our Global Recruiters
- Next, candidates will be invited to schedule a 30-45 minute interview with member(s) of our People Operations team
- Next, candidates will be invited to schedule a 30 minute interview with a member of our Events team
- After that, candidates will be invited to schedule a 45 minute interview with our Chief Culture Officer
- Next, candidates will be invited to schedule a 30 minute interview with a member of our executive team
- Finally, our CEO may choose to conduct a final interview
