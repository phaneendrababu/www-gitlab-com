---
layout: markdown_page
title: "GitHub"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Challenges
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## Summary
(drawn from [https://en.wikipedia.org/wiki/GitHub](https://en.wikipedia.org/wiki/GitHub))

GitHub is a collaborative code repository to host and review code, manage projects and build software. It offers all of the distributed version control and source code management (SCM) functionality of Git as well as adding its own features. It provides access control and several collaboration features such as bug tracking, feature requests, task management, and wikis for every project.

CI/CD is accomplished via integrations with other products, while GitLab has integrated CI/CD. In contrast, GitHub also offers application performance and server monitoring along with cycle analytics. GitLab includes static and dynamic security testing and container scanning.

GitHub does not come with a deployment platform and needs additional applications, like Heroku, in order to deploy applications. GitLab leverages [Kubernetes](/kubernetes) to create a seamless deployment experience in a single application. 

GitHub offers plans for both private repositories and free accounts which are commonly used to host open-source software projects. As of June 2018, GitHub reports having over 28 million users and 57 million repositories (including 28 million public repositories), making it the largest host of source code in the world.

On June 4, 2018, Microsoft announced it had reached an agreement to acquire GitHub for US$7.5 billion. The purchase is expected to close by the end of the year.

GitHuba is also offered in a self-managed version called GitHub Enterprise (GHE).

## Comments/Anecdotes
- Feedback from customers is that GitHub Enterprise (GHE) has trouble scalling. It seems like anyone nearing 2k users starts to run into issues. [GitLab is enterprise class](/enterprise-class/) and scales to > 32K users.
- 2018-09-10 - GitHub announces that [Microsoft Azure Pipelines are available in the GitHub Marketplace](https://blog.github.com/2018-09-10-azure-pipelines-now-available-in-github-marketplace/). This is in parallel with Microsoft (who recently acquired GitHub) announcing their Azure DevOps (rebranded VSTS/TFS). Related? Yes!
- 2018-09-10 - Microsoft announces the [addition of GitHub Pull Requests integrated directly into Visual Studio Code](https://code.visualstudio.com/blogs/2018/09/10/introducing-github-pullrequests). Visual Studio Code is [Microsoft's lightweight (but still not web based) code editor](https://azure.microsoft.com/en-us/products/visual-studio-code/).

## Resources
* [GitHub Website](https://www.github.com/)

## FAQs

### Distribution architecture
{:.no_toc}

Q:  Is GHE is based on a closed VM  
A: Yes - https://help.github.com/enterprise/2.13/admin/guides/installation/
“GitHub Enterprise is distributed as a virtual machine image that you install and manage within your existing infrastructure.”

Possible disadvantages of a closed VM:

Q:  a closed environment (could be a trojan inside!)  
A: Agree with assessment

Q:  Installation is quite complicated  
A: DISAGREE - single VM makes it easy to install, although less flexible  
See installation instructions at https://help.github.com/enterprise/2.13/admin/guides/installation/setting-up-a-github-enterprise-instance/

Q: upgrades and patching is complicated  
A: DISAGREE - VM appliance makes patching and upgrading relatively simple.  
See upgrade instructions at https://help.github.com/enterprise/2.13/admin/guides/installation/upgrading-github-enterprise/

Q:  bad thing off my head for using VM is I/O  
A: Not clear - In general yes. But things can be tuned to improve it a lot  
See https://academic.oup.com/comjnl/article/61/6/808/4259797 for general recent study on this.

Q: probably can't separate the installation to separated parts (in order to improve performance)  
A: DISAGREE - Can have VM act as different parts to scale per requirements. See https://help.github.com/enterprise/2.13/admin/guides/clustering/about-cluster-nodes/#cluster-design-recommendations and https://help.github.com/enterprise/2.13/admin/guides/clustering/initializing-the-cluster/ for more details.

## Comparison
