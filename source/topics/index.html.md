---
layout: markdown_page
title: "Industry Topics"
---
## Get insights into industry trends

- [DevOps](/devops)
- [20 years of open source](/20-years-open-source/)
- [Cloud native](/cloud-native/)